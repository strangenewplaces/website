---
title: Zur Alten Kaisermühle
type: restaurant
region: aut-9-vienna
tags:
- austrian
- grill 
paymentOptions:
- cash
website: http://www.kaisermuehle.at/
address: Fischerweg 21A, 1220 Vienna
pos:
    lat: 48.234702
    lng: 16.42645
---

If you’re looking for a nice lunch or dinner near the Danube, this restaurant
should be something for you. During the summer months you can also enjoy a
delicious BBQ in the outdoor seating area!
