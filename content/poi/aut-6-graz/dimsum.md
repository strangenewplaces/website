---
title: Dim Sum
type: restaurant
website: http://www.dimsum-graz.at/
address: Dietrichsteinplatz 2, 8010 Graz
phone: "+43 316 829 584"
region: aut-6-graz
pos:
    lat: 47.06655
    lng: 15.44898
paymentOptions:
    - cash
tags:
    - buffet
    - chinese
    - dimsum
    - dumplings
---

Nice, small chinese restaurant in in the heart of the city. If you're looking
for dim sum dumplings you are at the right place there!
